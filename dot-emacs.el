;;; package -- summary
;;; Commentary:
;;; This is my .emacs file
;;; Code:

;;; Enable MELPA package repository (for Emacs major versions > 24)
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  ;;(add-to-list 'package-archives '("melpa-stable"
  ;;. "https://stable.melpa.org/packages/") t)
  (package-initialize))

;; Custom packages
(package-initialize)

(defvar custom-packages '(elpy zenburn-theme flycheck auto-complete
			       yaml-mode dockerfile-mode auctex company
			       diff-hl magit web-mode swift-mode neotree ansible
			       rainbow-mode nginx-mode markdown-preview-mode
			       jinja2-mode impatient-mode flyspell-correct helm
			       darktooth-theme projectile jedi helm-projectile
			       py-isort irony company-irony all-the-icons
                               company-irony-c-headers go-mode go-autocomplete
			       clang-format pyenv-mode flyspell-popup flyspell
			       cpputils-cmake modern-cpp-font-lock racer
			       centaur-tabs rust-mode cargo flycheck-rust
			       cmake-mode irony flycheck-pycheckers doom-themes
			       helm-dash))

;; Fetch available packages
(unless package-archive-contents
  (package-refresh-contents))

(dolist (package custom-packages)
  (unless (package-installed-p package)
    (package-install package)))

(add-to-list 'load-path "~/.emacs.d/local/cc-mode-5.33/")
(require 'cc-mode)

;; hideshow-vis
(load-file "~/git/hideshowvis/hideshowvis.el")

;; General autocomplete
(ac-config-default)
;;(global-company-mode)

;; Visual settings
;; Theme
(load-theme 'dark-laptop t)
;; No toolbar
(tool-bar-mode -1)
;; Electric pair mode (automatch parens and quotes)
(electric-pair-mode 1)
;; Line/column numbering
(global-linum-mode 1)
(column-number-mode)
(global-hl-line-mode t)
;; Startup screen customization
(setq inhibit-startup-screen t)
(setq initial-scratch-message ";; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with C-x C-f and enter text in its buffer.
")
;; Maximize on startup
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(setq indent-tabs-mode nil)
(setq make-backup-files nil)
(show-paren-mode)
;; end Visual settings

;; Keybindings to hide/show text with hideshow
(global-set-key [C-tab] 'hs-hide-block)
(global-set-key [C-S-iso-lefttab] 'hs-show-block)

;; neotree
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq neo-window-fixed-size nil)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(defun neotree--fit-window (type path c)
  "Resize neotree window to fit contents based on TYPE, with PATH and C as unused variables as far as this function is concerned."
  (if (eq type 'directory)
      (neo-buffer--with-resizable-window
       (let ((fit-window-to-buffer-horizontally t))
         (fit-window-to-buffer)))))
;;(add-hook 'neo-change-root-hook #'neotree--fit-window)
(add-hook 'neo-enter-hook #'neotree--fit-window)
;; end neotree

;; magit
(global-set-key (kbd "C-x g") 'magit-status)

;; flyspell
(require 'flyspell-correct)
(setq flyspell-issue-message-flag nil)
(define-key flyspell-mode-map (kbd "C-;") #'flyspell-popup-correct)
(add-hook 'flyspell-mode-hook #'flyspell-popup-auto-correct-mode)
(defvar flyspell-popup-correct-delay 1.5)

;; jinja-2
(add-to-list 'auto-mode-alist '("\\.j2\\'" . jinja2-mode))

;; helm goodness
(require 'helm)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-h C-l") 'helm-locate)
(global-set-key (kbd "C-h C-g") 'helm-ls-git-ls)
(helm-mode)

(require 'helm-dash)
(add-hook 'python-mode-hook
	  (lambda ()
	    (interactive)
	    (setq-local helm-dash-docsets
			'("Python_3" "NumPy" "SciPy" "Jinja"))))

;; Centaur tabs
(require 'centaur-tabs)
(centaur-tabs-mode t)
(setq centaur-tabs-style "bar")
(setq centaur-tabs-set-bar 'left)
(setq centaur-tabs-set-icons t)
(global-set-key (kbd "M-]") 'centaur-tabs-forward)
(global-set-key (kbd "M-[") 'centaur-tabs-backward)
(global-set-key (kbd "C-M-]") 'centaur-tabs-move-current-tab-to-right)
(global-set-key (kbd "C-M-[") 'centaur-tabs-move-current-tab-to-left)
(global-set-key (kbd "s-]") 'centaur-tabs-forward-group)
(global-set-key (kbd "s-[") 'centaur-tabs-backward-group)

;; Handle the Custom variables in a different file
(setq custom-file "~/.emacs.d/custom.el")
(if (file-exists-p "~/.emacs.d/custom.el")
    (load-file "~/.emacs.d/custom.el"))

;; Language settings
;; Python
(require 'anaconda-mode)
(require 'company-anaconda)
(add-hook 'python-mode-hook (lambda ()
			      ;; (setq elpy-rpc-backend "jedi")
			      ;; (setq elpy-rpc-python-command "python3")
			      ;; (setq python-shell-interpreter "python3")
                              (set (make-local-variable 'company-backends)
				   '(company-anaconda))
			      ;; (setq jedi:complete-on-dot t)
			      ;; (jedi-mode)
			      ;; (jedi:setup)
			      (anaconda-mode)
			      (anaconda-eldoc-mode)
			      (company-mode)
			      (add-hook 'before-save-hook
					'py-isort-buffer nil 'local)))

(mapc (lambda (mode)
	(add-hook 'python-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))

(setenv "WORKON_HOME" "~/anaconda3/envs")
;; end Python

;; C++
;; Configure tabs to be two spaces in C/C++
(require 'clang-format)
(setq clang-format-style "file")
(setq c-default-style "linux")
(setq c-basic-offset 2)
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(mapc (lambda (mode)
	(add-hook 'c++-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))
(add-hook 'c++-mode-hook
	  (lambda ()
	    (add-hook 'before-save-hook
		      #'clang-format-buffer nil 'local)
	    (set (make-local-variable 'company-backends) '(company-rtags))))

;; end C++

;; company mode
(add-hook 'c++-mode-hook 'company-mode)
(add-hook 'c-mode-hook 'company-mode)

;; C mode for gnu99-style for loops
(defvar flycheck-gcc-language-standard "gnu99")
(mapc (lambda (mode)
	(add-hook 'c-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))

;; Go
(require 'company-go)
(with-eval-after-load 'go-mode
  (setenv "PATH" (concat "/usr/local/go/bin" ":" "~/go/bin" ":" (getenv "PATH")))
  (defconst godoc-and-godef-command "/usr/local/go/bin/godoc")
  (defconst godef-command "~/go/bin/godef")
  (defconst gofmt-command "/usr/local/go/bin/gofmt")
  (defconst company-go-gocode-command "~/go/bin/gocode")
  (local-set-key (kbd "M-g M-d") 'godoc-at-point))
(mapc (lambda (mode)
	(add-hook 'go-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))
(add-hook 'go-mode-hook
	  (lambda ()
            (set (make-local-variable 'company-backends) '(company-go))
            (company-mode)))
;; end Go

;; Lisp
(mapc (lambda (mode)
	(add-hook 'lisp-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))
;; end Lisp

;; ELisp
(mapc (lambda (mode)
	(add-hook 'emacs-lisp-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))
;; end ELisp

;; Shell
(mapc (lambda (mode)
	(add-hook 'sh-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))
;; end Shell

;; Markdown
(defvar markdown-command "/usr/sbin/pandoc")
(mapc (lambda (mode)
	(add-hook 'markdown-mode-hook mode))
      (list 'rainbow-mode
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))
;; end Markdown

;; Web development
(require 'web-mode)
(defvar web-development-mode-hooks '(web-mode-hook
				     css-mode-hook
				     javascript-mode-hook
				     markdown-mode-hook))

(mapc (lambda (language-mode-hook)
	(add-hook language-mode-hook 'rainbow-mode)
	(add-hook language-mode-hook 'hs-minor-mode)
	(add-hook language-mode-hook 'hideshowvis-enable)
	(add-hook language-mode-hook 'diff-hl-mode)) web-development-mode-hooks)

(add-hook 'web-mode-hook
	  (lambda ()
	    (setq web-mode-code-indent-offset 2)
	    (setq web-mode-css-indent-offset 2)
	    (setq web-mode-markup-indent-offset 2)))

;; Rust
(require 'rust-mode)
(require 'cargo)
;; Racer path
(setq racer-cmd "~/.cargo/bin/racer")
;; Path to Rust source
(setq racer-rust-src-path "~/git/rust/src")

(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'racer-mode-hook #'company-mode)
(add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
(mapc (lambda (mode)
	(add-hook 'rust-mode-hook mode))
      (list 'rainbow-mode
	    'hs-minor-mode
	    'hideshowvis-enable
	    'diff-hl-mode
	    'projectile-mode
	    'flycheck-mode))
;; end Rust

;; org-mode
(defvar org-use-sub-superscripts (quote {}))
(add-hook 'org-mode-hook 'flyspell-mode)

(add-hook 'org-mode-hook
	  (lambda ()
	    (plist-put org-format-latex-options :scale 2.5)))
;; end org-mode

(provide '.emacs)
;;; dot-emacs.el ends here
